import { GeodrivePage } from './app.po';

describe('geodrive App', function() {
  let page: GeodrivePage;

  beforeEach(() => {
    page = new GeodrivePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
