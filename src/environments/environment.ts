// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `angular-cli.json`.

export const environment = {
  production: false,
  API_URL: 'http://localhost',
  GOOGLE_MAPS_API_KEY: 'AIzaSyBK5HzEj7-wlKUY4aT5zvCNpc_yDMV9gT8',
  SNAP_TO_ROADS_URL: 'https://roads.googleapis.com/v1/snapToRoads',
};
