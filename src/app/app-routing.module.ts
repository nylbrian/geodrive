import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoginComponent } from './login/login.component';

const appRoutes: Routes = [
  { path: '', loadChildren: 'app/home/home.module#HomeModule', data : { sidebar: 'default' } },
  { path: 'login', component: LoginComponent },
  { path: '**', redirectTo: '' }
];
@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {}

export const AppRoutingComponents = [
  LoginComponent
];
