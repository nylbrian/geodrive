import { environment } from '../../environments/environment';
import { Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import { RequestHeaders } from '../shared/constants';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
// import 'rxjs/Rx';

@Injectable()
export class TeamService {
  private headers = new Headers(RequestHeaders);
  private apiUrl: string = environment.API_URL;

  constructor(private http: Http) {}

  getTeams(): Observable<any> {
    return this.http.post(
        `${this.apiUrl}/get-teams`,
        '',
        { headers: this.headers }
      )
      .map((response: Response) => {
        return response.json();
      });
  }

  getTeam(teamId: number): Observable<any> {
    return this.http.post(
        `${this.apiUrl}/get-team`,
        `teamId=${teamId}`,
        { headers: this.headers }
      )
      .map((response: Response) => {
        return response.json();
      });
  }
}
