import { Injectable } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';

import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class AlertService {
  private subject = new Subject<any>();
  private clearOnRouteChange = false;

  constructor(private router: Router) {
    router.events
      .filter(event => event instanceof NavigationStart)
      .subscribe(event => {
        if (this.clearOnRouteChange) {
          this.clearOnRouteChange = false;
        } else {
          this.subject.next();
        }
      });
  }

  success(message: string, clearOnRouteChange: boolean = false) {
    this.clearOnRouteChange = clearOnRouteChange;
    this.subject.next({ type: 'success', text: message });
  }

  error(message: string, clearOnRouteChange: boolean = false) {
    this.clearOnRouteChange = clearOnRouteChange;
    this.subject.next({ type: 'error', text: message });
  }

  clearMessage() {
    this.subject.next();
  }

  getMessage(): Observable<any> {
    return this.subject.asObservable();
  }
}
