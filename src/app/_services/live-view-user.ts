import { Coordinates } from './coordinates';

export class LiveViewUser {
  userId: number;
  coordinates: Array<Coordinates>;

  constructor(userId, coordinates) {
    this.userId = userId;
    this.coordinates = coordinates;
  }

  getCoordinates() {
    return this.coordinates;
  }

  getFirstCoordinate() {
    if (this.coordinates.length > 0) {
      return this.coordinates[0];
    }
  }

  getLastCoordinate() {
    if (this.coordinates.length > 0) {
      return this.coordinates[this.coordinates.length - 1];
    }
  }
}
