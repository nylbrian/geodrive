import { environment } from '../../environments/environment';
import { Injectable } from '@angular/core';
import { Headers, Http, Jsonp, Response, RequestOptions, URLSearchParams } from '@angular/http';
import { RequestHeaders } from '../shared/constants';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { Coordinates } from './coordinates';

@Injectable()
export class GoogleMapsService {
  private headers = new Headers(RequestHeaders);
  private apiUrl: string = environment.API_URL;

  constructor(
    private http: Http,
    private jsonp: Jsonp
  ) {}

  snapToRoad(path): Observable<any> {
    let pathJoined = this.createPath(path);
    const key = environment.GOOGLE_MAPS_API_KEY;

    return this.jsonp.get(
      `${environment.SNAP_TO_ROADS_URL}?interpolate=true&key=${key}&path=${pathJoined}&callback=JSONP_CALLBACK`)
      .map((response: Response) => {
        return response.json();
      });

      /*let params = new URLSearchParams();

    params.set('interpolate', 'true');
    params.set('key', key);
    params.set('path', pathJoined);

    const options = new RequestOptions({ search: params });*/

      /*.map((response: Response) => {
        return response.json();
      })
      .catch((error:any) => {
        return Observable.throw(error.status);
      });*/

    /*return this.http.get(environment.SNAP_TO_ROADS_URL, options)
      .map((response: Response) => {
        return response.json();
      })
      .catch((error:any) => {
        return Observable.throw(error.status);
      });*/
  }

  createPath(positionObject: Array<any>) {
    let pos: Array<string> = [];

    positionObject.forEach(position => {
      pos.push(`${position.latitude},${position.longitude}`);
    });

    return pos.join('|');
  }
}
