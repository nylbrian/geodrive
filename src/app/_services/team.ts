import { User } from './user';

export class Team {
  id: number;
  name: string;
  members: User[];

  getId() {
    return this.id;
  }

  getName() {
    return this.name;
  }

  setMembers(members) {
    this.members = members;
  }

  getMembers(members) {
    return this.members;
  }
}
