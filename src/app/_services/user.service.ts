import { environment } from '../../environments/environment';
import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { RequestHeaders } from '../shared/constants';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/Rx';

@Injectable()
export class UserService {
  private headers = new Headers(RequestHeaders);
  private apiUrl: string = environment.API_URL;

  constructor(private http: Http) {}

  login(email: string, password: string): Observable<any> {
    const loginUrl = `${this.apiUrl}/login`;
    //const loginUrl = 'app/assets/login.json';
    return this.http.post(
        loginUrl,
        `email=${email}&password=${password}`,
        { headers: this.headers }
      )
      .map((response: Response) => {
        return response.json();
      });
  }

  getUsers(): Observable<any> {
    const options = new RequestOptions();
    return this.http.post(
        `${this.apiUrl}/get-users`,
        '',
        { headers: this.headers }
      )
      .map((response: Response) => {
        return response.json();
      })
      .catch((error: any) => {
        return Observable.throw(error.status);
      });
  }

  getUser(userId: number): Observable<any> {
    const options = new RequestOptions();
    return this.http.post(
        `${this.apiUrl}/get-user`,
        `id=${userId}`,
        { headers: this.headers }
      )
      .map((response: Response) => {
        return response.json();
      })
      .catch((error: any) => {
        return Observable.throw(error.status);
      });
  }
}
