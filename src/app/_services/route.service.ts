import { environment } from '../../environments/environment';
import { Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import { RequestHeaders } from '../shared/constants';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/Rx';

@Injectable()
export class RouteService {
  private headers = new Headers(RequestHeaders);
  private apiUrl: string = environment.API_URL;

  constructor(private http: Http) {}

  getRouteForTheDay(userId: number): Observable<any> {
    return this.http.post(
        `${this.apiUrl}/get-route-for-the-day`,
        `id=${userId}`,
        { headers: this.headers }
      )
      .map((response: Response) => {
        return response.json();
      })
      .catch((error: any) => {
        return Observable.throw(error.status);
      });
  }

  getRouteOfTeam(teamId: number): Observable<any> {
    return this.http.post(
        `${this.apiUrl}/get-route-of-team`,
        `teamId=${teamId}`,
        { headers: this.headers }
      )
      .map((response: Response) => {
        return response.json();
      })
      .catch((error: any) => {
        return Observable.throw(error.status);
      });
  }
}
