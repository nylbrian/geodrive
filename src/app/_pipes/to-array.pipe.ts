import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'ToArray' })
export class ToArray implements PipeTransform {
  transform (value, args) {
    console.log(value);
    return Array.from(value);
  }
}
