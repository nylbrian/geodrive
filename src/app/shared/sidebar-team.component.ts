import { Component, Input } from '@angular/core';
import { TeamService } from '../_services/team.service';

@Component({
  selector: 'sidebar-team-component',
  templateUrl: './sidebar-team.component.html',
  providers: [
    TeamService
  ]
})
export class SidebarTeamComponent {
  @Input() teams: any;
  @Input() url: string;
}
