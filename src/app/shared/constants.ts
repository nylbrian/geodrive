export const SideBarTypes = {
  default: 'default',
  liveView: 'liveView',
  liveViewIndividual: 'liveViewIndividual',
  liveViewTeam: 'liveViewTeam',
  report: 'report',
  reportGraphical: 'reportGraphical',
  reportGraphicalIndividual: 'reportGraphicalIndividual',
  reportGraphicalTeam: 'reportGraphicalTeam',
  reportMap: 'reportMap',
  reportMapIndividual: 'reportMapIndividual',
  reportMapTeam: 'reportMapTeam',
};

export const RequestHeaders = {
  'Content-Type': 'application/x-www-form-urlencoded'
};

export const MapStyle = [
  {
    "featureType": "all",
    "stylers": [
      {
        "color": "#2b2b2b"
      }
    ]
  },
  {
    "featureType": "road",
    "stylers": [
      {
        "color": "#333333"
      }
    ]
  },
  {
    "elementType": "labels.text",
    "stylers": [
      {
        "color": "#888888"
      }
    ]
  },
  {
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#333333"
      }
    ]
  },
  {
    "featureType": "water",
    "stylers": [
      {
        "color": "#676767"
      }
    ]
  }
];