import { Component, Input } from '@angular/core';

@Component({
  selector: 'sidebar-individual-component',
  templateUrl: './sidebar-individual.component.html',
  providers: []
})
export class SidebarIndividualComponent {
  @Input() users: any;
  @Input() url: string;
}
