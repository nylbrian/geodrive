import { GoogleMapsAPIWrapper } from 'angular2-google-maps/core/services/google-maps-api-wrapper';
import { Directive,  Input} from '@angular/core';
declare var google: any;

@Directive({
  selector: 'sebm-google-map-directions'
})
export class DirectionsMapDirective {
  @Input() origin;
  @Input() destination;
  @Input() waypoints;

  public directionsService: any = {};
  public directionsDisplay: any = {};

  constructor (private gmapsApi: GoogleMapsAPIWrapper) {

  }

  ngOnInit() {
    this.gmapsApi.getNativeMap().then(map => {
      this.directionsService = new google.maps.DirectionsService;
      this.directionsDisplay = new google.maps.DirectionsRenderer;
      this.directionsDisplay.setMap(map);
      this.route();
    });
  }

  ngOnChanges(changes:any) {
    this.route();
  }

  route() {
    if (!this.directionsService.route) {
      return false;
    }

    this.directionsService.route({
      origin: this.origin,
      destination: this.destination,
      waypoints: [],
      optimizeWaypoints: true,
      travelMode: google.maps.DirectionsTravelMode.DRIVING,
      }, (response, status) => {
        if (status === 'OK') {
          this.directionsDisplay.setDirections(response);
        } else {
          window.alert('Directions request failed due to ' + status);
        }
    });
  }
}

