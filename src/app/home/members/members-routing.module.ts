import { SideBarTypes } from '../../shared/constants';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { MembersComponent } from './members.component';
import { MembersAddComponent } from './members-add.component';
import { MembersEditComponent } from './members-edit.component';

const appRoutes: Routes = [
  { path: '', component: MembersComponent },
  { path: 'edit/:id', component: MembersEditComponent },
  { path: 'add', component: MembersAddComponent }
];
@NgModule({
  imports: [
    RouterModule.forChild(appRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class MembersRoutingModule {}

export const MembersRoutingComponents = [
  MembersComponent,
  MembersEditComponent,
  MembersAddComponent
];
