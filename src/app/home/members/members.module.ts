import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {
  MembersRoutingModule,
  MembersRoutingComponents
} from './members-routing.module';

@NgModule({
  declarations: [
    MembersRoutingComponents
  ],
  imports: [
    MembersRoutingModule
  ],
  providers: []
})
export class MembersModule {}
