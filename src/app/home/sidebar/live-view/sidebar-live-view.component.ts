import { Component, Input } from '@angular/core';
import { TeamService } from '../../../_services/team.service';
import { UserService } from '../../../_services/user.service';

@Component({
  selector: 'sidebar-live-view',
  templateUrl: './sidebar-live-view.component.html',
  providers: [
    TeamService,
    UserService
  ]
})
export class SidebarLiveViewComponent {
  @Input() teamUrl: string;
  @Input() individualUrl: string;

  public teams: Array<any>;
  public users: Array<any>;

  constructor(
    private teamService: TeamService,
    private userService: UserService) {
  }

  ngOnInit() {
    this.getTeams();
    this.getUsers();
  }

  getUsers() {
    this.userService.getUsers().
      subscribe(
        response => {
          if (response.status === 1) {
            this.users = response.data;
          }
        },
        error => {

        }
      )
  }

  getTeams() {
    this.teamService.getTeams()
      .subscribe(
        response => {
          if (response.status === 1) {
            this.teams = response.data;
          }
        },
        error => {

        }
      );
  }


}
