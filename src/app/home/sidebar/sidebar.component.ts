import { SideBarTypes } from '../../shared/constants';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';

@Component({
  selector: 'sidebar',
  templateUrl: './sidebar.component.html',
})
export class SidebarComponent implements OnInit {
  public sidebarTypes = SideBarTypes;
  public type: string;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this.router.events
      .filter(event => event instanceof NavigationEnd)
      .subscribe(event => {
        const activatedRoute = this.activatedRoute;
        const activeRoutes: ActivatedRoute[] = this.activatedRoute.children;

        if (activeRoutes.length <= 0) {
          return;
        }

        let activeRoute: ActivatedRoute = activeRoutes[0];

        while (activeRoute.firstChild) {
          activeRoute = activeRoute.firstChild;
        }
        const routeData: any = activeRoute.snapshot.data;

        if (routeData.sidebar in SideBarTypes) {
          this.type = routeData.sidebar;
        } else {
          this.type = SideBarTypes.default;
        }
      });
  }
}
