import 'rxjs/add/operator/filter';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule, HomeRoutingComponents } from './home-routing.module';

import { NavigationComponent } from './navigation/navigation.component';

import { SidebarIndividualComponent } from '../shared/sidebar-individual.component';
import { SidebarTeamComponent } from '../shared/sidebar-team.component';

import { SidebarComponent } from './sidebar/sidebar.component';
import { SidebarDefaultComponent } from './sidebar/default/sidebar-default.component';
import { SidebarLiveViewComponent } from './sidebar/live-view/sidebar-live-view.component';

import { SidebarLiveViewIndividualComponent }
  from './sidebar/live-view/individual/sidebar-live-view-individual.component';

import { SidebarLiveViewTeamComponent }
  from './sidebar/live-view/team/sidebar-live-view-team.component';

import { SidebarReportComponent } from './sidebar/report/sidebar-report.component';

import { SidebarReportGraphicalComponent }
  from './sidebar/report/graphical/sidebar-report-graphical.component';

import { SidebarReportGraphicalIndividualComponent } from
  './sidebar/report/graphical/individual/sidebar-report-graphical-individual.component';

import { SidebarReportGraphicalTeamComponent } from
  './sidebar/report/graphical/team/sidebar-report-graphical-team.component';

import { SidebarReportMapComponent }
  from './sidebar/report/map/sidebar-report-map.component';

import { SidebarReportMapIndividualComponent }
  from './sidebar/report/map/individual/sidebar-report-map-individual.component';

import { SidebarReportMapTeamComponent }
  from './sidebar/report/map/team/sidebar-report-map-team.component';

@NgModule({
  declarations: [
    NavigationComponent,
    SidebarIndividualComponent,
    SidebarTeamComponent,
    SidebarComponent,
    SidebarDefaultComponent,
    SidebarLiveViewComponent,
    SidebarLiveViewIndividualComponent,
    SidebarLiveViewTeamComponent,
    SidebarReportComponent,
    SidebarReportGraphicalComponent,
    SidebarReportGraphicalIndividualComponent,
    SidebarReportGraphicalTeamComponent,
    SidebarReportMapComponent,
    SidebarReportMapIndividualComponent,
    SidebarReportMapTeamComponent,
    HomeRoutingComponents,
  ],
  imports: [
    CommonModule,
    HomeRoutingModule
  ],
  providers: []
})
export class HomeModule {}
