import { SideBarTypes } from '../shared/constants';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './home.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SettingsComponent } from './settings/settings.component';

const appRoutes: Routes = [
  {
    path: '',
    component: HomeComponent,
    children: [
      {
        path: '',
        component: DashboardComponent,
        data: { sidebar: SideBarTypes.default }
      },
      {
        path: 'settings',
        component: SettingsComponent,
        data: { sidebar: SideBarTypes.default }
      },
      {
        path: 'members',
        loadChildren: 'app/home/members/members.module#MembersModule',
        data: { sidebar: SideBarTypes.default }
      },
      {
        path: 'clients',
        loadChildren: 'app/home/clients/clients.module#ClientsModule',
        data: { sidebar: SideBarTypes.default }
      },
      {
        path: 'live-view',
        loadChildren: 'app/home/live-view/live-view.module#LiveViewModule',
      },
      {
        path: 'report',
        loadChildren: 'app/home/report/report.module#ReportModule',
      }
    ]
  }
];

export const HomeRoutingComponents = [
  HomeComponent,
  DashboardComponent,
  SettingsComponent
];

@NgModule({
  imports: [
    RouterModule.forChild(appRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class HomeRoutingModule {}
