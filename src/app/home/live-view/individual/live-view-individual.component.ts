import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RouteService } from '../../../_services/route.service';
import { UserService } from '../../../_services/user.service';
import { GoogleMapsService } from '../../../_services/google-maps.service';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/mergeMap';

import { MapStyle } from '../../../shared/constants';

@Component({
  templateUrl: './live-view-individual.component.html',
  providers: [
    RouteService,
    UserService,
    GoogleMapsService
  ],
})
export class LiveViewIndividualComponent {
  public lat: number;
  public lng: number;
  public user: any = {};
  public id: number;
  public userCoordinates: Array<any> = [];
  public snappedPoints: Array<any> = [];
  public userDoesNotExists: boolean = false;
  public userHasNoRoute: boolean = false;
  public loading: boolean = true;
  public mapStyle: any;
  public marker: any;
  private subscription;

  constructor(
    private gmapsService: GoogleMapsService,
    private route: ActivatedRoute,
    private routeService: RouteService,
    private userService: UserService) {}

  ngOnInit() {

    this.mapStyle = MapStyle;

    this.route.params.subscribe(params => {
      this.id = params['id'];
      this.initialize();
    });
  }

  centerMap() {
    if (this.snappedPoints.length > 0) {
      const coordinates = this.snappedPoints[this.snappedPoints.length -1];
      this.lat = coordinates.location.latitude;
      this.lng = coordinates.location.longitude;
    } else if (this.userCoordinates.length > 0) {
      this.lng = this.userCoordinates[this.userCoordinates.length - 1].longitude;
      this.lat = this.userCoordinates[this.userCoordinates.length - 1].latitude;
    }

  }

  initialize() {
    this.userService.getUser(this.id)
      .subscribe(
        response => {
          this.user = response.data;
          this.marker = {
            name: `${this.user.first_name} ${this.user.last_name}`,
          }
          this.watchCoordinates();
        },
        error => {
          this.userDoesNotExists = true;
        }
      );
  }

  watchCoordinates() {
    this.subscription = Observable.interval(2000)
      .switchMap(() => this.routeService.getRouteForTheDay(this.id))
      .subscribe(
        response => {
          this.loading = false;

          if (response.data.length <= 0) {
            this.userHasNoRoute = true;
            return false;
          }

          this.userHasNoRoute = false;

          if (response.data.length === this.userCoordinates.length) {
            return;
          }

          this.userCoordinates = response.data;
          this.gmapsService.snapToRoad(this.userCoordinates)
            .subscribe(
              response => {
                if (response.snappedPoints) {
                  this.snappedPoints = response.snappedPoints;
                }

                this.centerMap();
              },
              error => {

              }
            );
        },
        error => {
          this.userHasNoRoute = true;
        }
      );
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
