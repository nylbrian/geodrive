import { SideBarTypes } from '../../shared/constants';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LiveViewComponent } from './live-view.component';
import { LiveViewIndividualComponent } from './individual/live-view-individual.component';
import { LiveViewTeamComponent } from './team/live-view-team.component';

const appRoutes: Routes = [
  {
    path: '',
    component: LiveViewComponent,
    data: { sidebar: SideBarTypes.liveView }
  },
  {
    path: 'individual/:id',
    component: LiveViewIndividualComponent,
    data: {
      sidebar: SideBarTypes.liveViewIndividual
    }
  },
  {
    path: 'team/:id',
    component: LiveViewTeamComponent,
    data: {
      sidebar: SideBarTypes.liveViewTeam
    }
  }
];
@NgModule({
  imports: [
    RouterModule.forChild(appRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class LiveViewRoutingModule {}

export const LiveViewRoutingComponents = [
  LiveViewComponent,
  LiveViewIndividualComponent,
  LiveViewTeamComponent
];
