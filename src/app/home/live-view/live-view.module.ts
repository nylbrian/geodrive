import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AgmCoreModule } from 'angular2-google-maps/core';
import { environment } from '../../../environments/environment';
import { DirectionsMapDirective } from '../../shared/directions-map.directive';
import { ToArray } from '../../_pipes/to-array.pipe';

import {
  LiveViewRoutingModule,
  LiveViewRoutingComponents,
} from './live-view-routing.module';

@NgModule({
  declarations: [
    LiveViewRoutingComponents,
    DirectionsMapDirective,
    ToArray
  ],
  imports: [
    CommonModule,
    LiveViewRoutingModule,
    AgmCoreModule.forRoot({
      apiKey: environment.GOOGLE_MAPS_API_KEY
    })
  ],
  providers: []
})
export class LiveViewModule {}
