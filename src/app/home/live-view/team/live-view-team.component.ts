import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RouteService } from '../../../_services/route.service';
import { TeamService } from '../../../_services/team.service';
import { GoogleMapsService } from '../../../_services/google-maps.service';

import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import 'rxjs/add/operator/mergeMap';

import { MapStyle } from '../../../shared/constants';

@Component({
  templateUrl: './live-view-team.component.html',
  providers: [
    RouteService,
    TeamService,
    GoogleMapsService
  ],
})
export class LiveViewTeamComponent implements OnInit {
  private teamId: number;
  private usersWatcher: Subscription;
  private userCoordinates: any = {};

  public userSnappedPoints: any = {};
  public userMarker: any = {};
  public users: any = {};
  public teamDoesNotExists: boolean = false;
  public name: string;
  public lng: number;
  public lat: number;
  public loading: boolean = true;
  public teamHasNoRoute: boolean = false;
  public subscribedUsers: Array<any> = [];
  public mapStyle: any;

  constructor(
    private gmapsService: GoogleMapsService,
    private route: ActivatedRoute,
    private routeService: RouteService,
    private teamService: TeamService
  ) {}

  ngOnInit() {
    
    this.mapStyle = MapStyle;

    this.route.params.subscribe(params => {
      this.teamId = params['id'];
      this.initialize();
    });
  }

  initialize() {
    this.teamService.getTeam(this.teamId)
      .subscribe(
        response => {
          if (response.status === 1 && response.data.length > 0) {
            this.name = response.data[0].name;
            this.watchUsers();
          } else {
            this.teamDoesNotExists = true;
          }
        },
        error => {
          this.teamDoesNotExists = true;
        }
      );
  }

  watchUsers() {
    this.usersWatcher = Observable
      .interval(2000)
      .switchMap(() => this.routeService.getRouteOfTeam(this.teamId))
      .subscribe(
        response => {
          this.loading = false;

          if (response.status !== 1) {
            return;
          }

          if (response.data.length === Object.keys(this.users).length) {
            return;
          }

          response.data.forEach(user => {
            if (this.users[user.userId]) {
              return;
            }

            this.users[user.userId] = user;
            this.watchUserCoordinates(user);
          });
        },
        error => {
          this.teamHasNoRoute = true;
        }
      );
  }

  watchUserCoordinates(user) {
    this.userCoordinates[user.userId] = [];
    this.subscribedUsers.push(
      Observable
        .interval(2000)
        .switchMap(() => this.routeService.getRouteForTheDay(user.userId))
        .subscribe(
          response => {
            if (response.data.length <= 0) {
              return false;
            }

            if (response.data.length === this.userCoordinates[user.userId].length) {
              return;
            }

            this.userCoordinates[user.userId] = response.data;
            this.gmapsService.snapToRoad(this.userCoordinates[user.userId])
              .subscribe(
                response => {
                  if (response.snappedPoints) {
                    this.userSnappedPoints[user.userId] = response.snappedPoints;
                    this.userMarker[user.userId] = this.getMarker(response.snappedPoints, user);
                    this.centerMap();
                  }
                },
                error => {

                }
              );
          },
          error => {

          }
        )
    );
  }

  centerMap() {
    if (!this.lng && !this.lat) {
      const snappedPointKeys = Object.keys(this.userSnappedPoints);
      const userCoordinateKeys = Object.keys(this.userCoordinates);

      if (snappedPointKeys.length > 0) {
        const coordinates = this.userSnappedPoints[snappedPointKeys[0]];
        const key = coordinates.length - 1;
        this.lat = coordinates[key].location.latitude;
        this.lng = coordinates[key].location.longitude;
      } else if (userCoordinateKeys.length > 0) {
        this.lat = this.userCoordinates[userCoordinateKeys[0]].latitude;
        this.lng = this.userCoordinates[userCoordinateKeys[0]].longitude;
      }
    }
  }

  getMarker(coordinates: any, user: any) {
    let marker: any = {};

    if (coordinates.length > 0) {
      const coord = this.userSnappedPoints[user.userId];
      const key = coord.length - 1;
      marker = {
        lat: coord[key].location.latitude,
        lng: coord[key].location.longitude
      }
    } else {
      const coord = this.userCoordinates[user.userId];
      const key = coord.length - 1;
      marker = {
        lat: coord[key].latitude,
        lng: coord[key].longitude,
      };
    }

    marker.name = `${user.firstName} ${user.lastName}`;
    return marker;
  }

  ngOnDestroy() {
    this.usersWatcher.unsubscribe();

    for (let i = 0; i < this.subscribedUsers.length; i++) {
      this.subscribedUsers[i].unsubscribe();
    }
  }

  toArray(obj: any) {
    return (<any>Object).values(obj);
  }

}
