import { Component, Input, Output, EventEmitter } from '@angular/core';
import { GoogleMapsService } from '../../_services/google-maps.service';
import { LiveViewUser } from '../../_services/live-view-user';

@Component({
  templateUrl: './live-view.component.html',
  providers: [
    GoogleMapsService,
  ]
})
export class LiveViewComponent {
  public userList: Array<LiveViewUser> = [];
  public userData: any;
  public users: Array<any>;
  public lat: number;
  public lng: number;

  constructor(private gmapsService: GoogleMapsService) {}

}
