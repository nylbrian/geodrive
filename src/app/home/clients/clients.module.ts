import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { 
  ClientsRoutingModule, 
  ClientsRoutingComponents 
} from './clients-routing.module';

@NgModule({
  declarations: [
    ClientsRoutingComponents
  ],
  imports: [
    ClientsRoutingModule
  ],
  providers: []
})
export class ClientsModule {}
