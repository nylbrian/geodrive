import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ClientsListComponent } from './clients-list.component';
import { ClientsAgentsComponent } from './clients-agents.component';
import { ClientsAddComponent } from './clients-add.component';

const appRoutes: Routes = [
  { path: '', component: ClientsListComponent },
  { path: ':id/agents', component: ClientsAgentsComponent },
  { path: 'add', component: ClientsAddComponent }
];
@NgModule({
  imports: [
    RouterModule.forChild(appRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class ClientsRoutingModule {}

export const ClientsRoutingComponents = [
  ClientsListComponent,
  ClientsAgentsComponent,
  ClientsAddComponent
];
