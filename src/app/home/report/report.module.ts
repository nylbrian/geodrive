import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { environment } from '../../../environments/environment';

import {
  ReportRoutingModule,
  ReportRoutingComponents
} from './report-routing.module';

@NgModule({
  declarations: [
    ReportRoutingComponents
  ],
  imports: [
    ReportRoutingModule
  ],
  providers: []
})
export class ReportModule {}
