import { SideBarTypes } from '../../shared/constants';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ReportComponent } from './report.component';
import { ReportGraphicalComponent } from './graphical/report-graphical.component';
import { ReportGraphicalIndividualComponent }
  from './graphical/individual/report-graphical-individual.component';
import { ReportGraphicalTeamComponent }
  from './graphical/team/report-graphical-team.component';
import { ReportMapComponent } from './map/report-map.component';
import { ReportMapIndividualComponent }
  from './map/individual/report-map-individual.component';
import { ReportMapTeamComponent }
  from './map/team/report-map-team.component';

const appRoutes: Routes = [
  {
    path: '',
    component: ReportComponent,
    data: { sidebar: SideBarTypes.report }
  },
  {
    path: 'graphical',
    component: ReportGraphicalComponent,
    data: {
      sidebar: SideBarTypes.reportGraphical
    }
  },
  {
    path: 'graphical/individual/:id',
    component: ReportGraphicalIndividualComponent,
    data: {
      sidebar: SideBarTypes.reportGraphicalIndividual
    }
  },
  {
    path: 'graphical/team/:id',
    component: ReportGraphicalTeamComponent,
    data: {
      sidebar: SideBarTypes.reportGraphicalTeam
    }
  },
  {
    path: 'map',
    component: ReportMapComponent,
    data: {
      sidebar: SideBarTypes.reportMap
    }
  },
  {
    path: 'map/individual/:id',
    component: ReportMapIndividualComponent,
    data: {
      sidebar: SideBarTypes.reportMapIndividual
    }
  },
  {
    path: 'map/team/:id',
    component: ReportMapTeamComponent,
    data: {
      sidebar: SideBarTypes.reportMapTeam
    }
  }
];
@NgModule({
  imports: [
    RouterModule.forChild(appRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class ReportRoutingModule {}

export const ReportRoutingComponents = [
  ReportComponent,
  ReportGraphicalComponent,
  ReportGraphicalIndividualComponent,
  ReportGraphicalTeamComponent,
  ReportMapComponent,
  ReportMapIndividualComponent,
  ReportMapTeamComponent
];
