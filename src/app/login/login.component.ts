import { Component } from '@angular/core';
import { UserService } from '../_services/user.service';
import { AlertService } from '../_services/alert.service';
import { Router } from '@angular/router';

@Component({
  templateUrl: './login.component.html',
  providers: [
    UserService,
    AlertService
  ]
})
export class LoginComponent {
  public email: string;
  public password: string;
  public errorMessage: string;
  public formDisabled: boolean = false;

  constructor(
    private router: Router,
    private userService: UserService,
    private alertService: AlertService
  ) {}

  login() {
    this.formDisabled = true;
    this.alertService.clearMessage();
    this.userService.login(this.email, this.password)
      .subscribe(
        response => {
          if (response.status === 1) {
            this.alertService.success('Successfully logged in!');
            setTimeout(() => {
              this.router.navigate(['/']);
            }, 2000);  //5s
          } else {
            this.alertService.error('Incorrect username / password');
            this.formDisabled = false;
          }
        },
        error => {
          this.alertService.error('An unknown error has occured. Please try again');
          this.formDisabled = false;
        }
      );
  }
}
